package main

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

type Message struct {
	Id           bson.ObjectId `bson:"_id"`
	SectionId    int64         `bson:"sectionId"`
	DeploymentId int64         `bson:"deploymentId"`
	StudentId    int64         `bson:"studentId"`
	QuestionId   int64         `bson:"questionId"`
	To           string        `bson:"to"`
	From         string        `bson:"from"`
	Body         string        `bson:"body"`
	Read         bool          `bson:"read"`
	Answered     bool          `bson:"answered"`
	Originator   string        `bson:"originator"`
	LastUpdated  time.Time     `bson:"lastUpdated"`
	Created      time.Time     `bson:"created"`
}

type Conversation struct {
	Id           bson.ObjectId `bson:"_id"`
	SectionId    int64         `bson:"sectionId"`
	DeploymentId int64         `bson:"deploymentId"`
	StudentId    int64         `bson:"studentId"`
	LastUpdated  time.Time     `bson:"updatedAt"`
	Created      time.Time     `bson:"createdAt"`
	Questions    []Question    `bson:"questions"`
}

type Question struct {
	Id       int64             `bson:"id"`
	Answered bool              `bson:"isAnswered"`
	Messages []QuestionMessage `bson:"messages"`
}

type QuestionMessage struct {
	To          string    `bson:"to"`
	From        string    `bson:"from"`
	Body        string    `bson:"body"`
	Read        bool      `bson:"isRead"`
	Originator  string    `bson:"originator"`
	LastUpdated time.Time `bson:"updatedAt"`
	Created     time.Time `bson:"createdAt"`
}

func makeQuestionFromMessage(m *Message) Question {
	q := Question{
		Id:       m.QuestionId,
		Answered: m.Answered,
		Messages: []QuestionMessage{makeQuestionMessageFromMessage(m)},
	}

	return q
}

func makeQuestionMessageFromMessage(m *Message) QuestionMessage {
	qm := QuestionMessage{
		To:          m.To,
		From:        m.From,
		Body:        m.Body,
		Read:        m.Read,
		Originator:  m.Originator,
		LastUpdated: m.LastUpdated,
		Created:     m.Created,
	}

	return qm
}

func MessageToConversation(m *Message) *Conversation {
	var res Conversation

	res.Id = bson.NewObjectId()
	res.SectionId = m.SectionId
	res.DeploymentId = m.DeploymentId
	res.StudentId = m.StudentId
	res.LastUpdated = m.LastUpdated
	res.Created = m.Created

	qm := makeQuestionMessageFromMessage(m)
	q := makeQuestionFromMessage(m)
	q.Messages = []QuestionMessage{qm}
	res.Questions = []Question{q}

	return &res
}
