package main

import (
	//"github.com/davecgh/go-spew/spew"
	"testing"
	"time"
)

func TestUnitConvertMessage2Conversation(t *testing.T) {

	m := Message{
		SectionId:    1,
		DeploymentId: 2,
		StudentId:    3,
		To:           "Andrew Austin",
		From:         "Briana Austin",
		Body:         "How are you?",
		Read:         false,
		Answered:     false,
		Originator:   "student",
		LastUpdated:  time.Now(),
		Created:      time.Now(),
	}

	res := MessageToConversation(&m)

	if res.SectionId != m.SectionId {
		t.Error("Conversation SectionId does not match original message SectionId")
	}

	if res.DeploymentId != m.DeploymentId {
		t.Error("Conversation DeploymentId does not match original message DeploymentId")
	}

	if res.StudentId != m.StudentId {
		t.Error("Conversation StudentId does not match original message StudentId")
	}

	if res.LastUpdated != m.LastUpdated {
		t.Error("Conversation LastUpdated does not match original message LastUpdated")
	}

	if res.Created != m.Created {
		t.Error("Conversation Created does not match original message Created")
	}

	if len(res.Questions) != 1 {
		t.Error("The number of questions in the conversation does not equal 1")
	}

	if len(res.Questions[0].Messages) != 1 {
		t.Error("The number of messages in the question does not equal 1")
	}

}

func TestAddMessageToQuestion(t *testing.T) {
	m := Message{
		SectionId:    1,
		DeploymentId: 2,
		StudentId:    3,
		QuestionId:   42,
		To:           "Andrew Austin",
		From:         "Briana Austin",
		Body:         "How are you?",
		Read:         false,
		Answered:     false,
		Originator:   "student",
		LastUpdated:  time.Now(),
		Created:      time.Now(),
	}

	res := MessageToConversation(&m)

	qm := QuestionMessage{
		To:          m.To,
		From:        m.From,
		Body:        "THIS IS A TEST!",
		Read:        m.Read,
		Originator:  m.Originator,
		LastUpdated: m.LastUpdated,
		Created:     m.Created,
	}
	addMessageToQuestion(42, qm, res)
	if len(res.Questions[0].Messages) != 2 {
		t.Error("Add message to question did not add message")
	}
}
