package main

import (
	"fmt"
	//"github.com/davecgh/go-spew/spew"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

type settings struct {
	MgoConnectionString string
}

var session *mgo.Session
var aytConversations *mgo.Collection
var aytMessages *mgo.Collection
var s settings

func migrateMessageToConversation(m *Message) {
	// Check to see if a Conversation exists that has the same DeploymentId and SectionId
	convo := getConversationIfExists(m)
	if convo != nil {
		//fmt.Println("Conversation Found!")
		// If a conversation does exist, check the QuestionId to see if it already exists.
		if questionExists(convo, m) {
			// If a question does exist, add the new message to the end of the list
			newMessage := makeQuestionMessageFromMessage(m)
			addMessageToQuestion(m.QuestionId, newMessage, convo)
			if m.LastUpdated.After(convo.LastUpdated) {
				convo.LastUpdated = m.LastUpdated
			}
			if !m.Answered {
				for index, elem := range convo.Questions {
					if elem.Id == m.QuestionId {
						convo.Questions[index].Answered = false
						break
					}
				}
			}
			err := aytConversations.UpdateId(convo.Id, convo)
			if err != nil {
				panic(err)
			}
		} else {
			// If a question does not exist, create it and add a new message to that question
			newQuestion := makeQuestionFromMessage(m)
			convo.Questions = append(convo.Questions, newQuestion)
			if m.LastUpdated.After(convo.LastUpdated) {
				convo.LastUpdated = m.LastUpdated
			}
			err := aytConversations.UpdateId(convo.Id, convo)
			if err != nil {
				panic(err)
			}
		}
	} else {
		//fmt.Println("Conversation Not Found")
		// If a conversation does not exist, use MessageToConversation() to get the new document and write it to the db
		convo := MessageToConversation(m)
		err := aytConversations.Insert(convo)
		if err != nil {
			panic(err)
		}
	}
	// Checks need to be done to update the collection updatedAt and the question answered fieget
}

func getConversationIfExists(m *Message) *Conversation {
	var convo Conversation
	err := aytConversations.Find(bson.M{"studentId": m.StudentId, "sectionId": m.SectionId, "deploymentId": m.DeploymentId}).One(&convo)
	if err != nil {
		if err == mgo.ErrNotFound {
			return nil
		} else {
			panic(err)
		}
	}

	return &convo
}

func questionExists(convo *Conversation, m *Message) bool {
	for _, elem := range convo.Questions {
		if elem.Id == m.QuestionId {
			return true
		}
	}
	return false
}

func addMessageToQuestion(id int64, qm QuestionMessage, c *Conversation) {
	for index, elem := range c.Questions {
		if elem.Id == id {
			c.Questions[index].Messages = append(c.Questions[index].Messages, qm)
			break
		}
	}
}

func init() {
	file, err := ioutil.ReadFile("./config.yml")
	if err != nil {
		panic(err)
	}
	err = yaml.Unmarshal(file, &s)
	if err != nil {
		panic(err)
	}
}

func main() {
	var err error
	var res Message

	fmt.Println("Connecting to " + s.MgoConnectionString)
	session, err = mgo.Dial(s.MgoConnectionString)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	aytConversations = session.DB("messages").C("Conversations")
	aytMessages = session.DB("messages").C("aytmessages")
	// Iterate through each document in the collection
	iter := aytMessages.Find(nil).Iter()
	fmt.Println("Starting migration...")
	for iter.Next(&res) {
		migrateMessageToConversation(&res)
	}
	fmt.Println("Migration complete")
}
