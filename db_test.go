package main

import (
	//"github.com/davecgh/go-spew/spew"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"testing"
	"time"
)

func TestMakeConversationIfExists(t *testing.T) {
	var err error

	session, err = mgo.Dial("localhost")
	if err != nil {
		t.Error(err)
	}

	aytMessages = session.DB("messages_test").C("aytmessages")
	_ = aytMessages.DropCollection()

	aytConversations = session.DB("messages_test").C("aytconversations")
	_ = aytConversations.DropCollection()

	m := Message{
		Id:           bson.NewObjectId(),
		SectionId:    1,
		DeploymentId: 2,
		StudentId:    3,
		QuestionId:   17,
		To:           "Briana Austin",
		From:         "Andrew Austin",
		Body:         "I'm fine",
		Read:         false,
		Answered:     false,
		Originator:   "faculty",
		LastUpdated:  time.Now(),
		Created:      time.Now(),
	}

	err = aytMessages.Insert(m)
	if err != nil {
		t.Error(err)
	}

	if getConversationIfExists(&m) != nil {
		t.Error("The conversation should not exist yet")
	} else {
		convo := MessageToConversation(&m)
		err := aytConversations.Insert(convo)
		if err != nil {
			t.Error(err)
		}

		if getConversationIfExists(&m) == nil {
			t.Error("The conversation does not exist, but should")
		}
	}
}

func TestConversationExistsWithOthers(t *testing.T) {
	var err error

	session, err = mgo.Dial("localhost")
	if err != nil {
		t.Error(err)
	}

	aytMessages = session.DB("messages_test").C("aytmessages")
	_ = aytMessages.DropCollection()

	aytConversations = session.DB("messages_test").C("aytconversations")
	_ = aytConversations.DropCollection()

	m := Message{
		SectionId:    1,
		DeploymentId: 2,
		StudentId:    3,
		To:           "Briana Austin",
		From:         "Andrew Austin",
		Body:         "I'm fine",
		Read:         false,
		Answered:     false,
		Originator:   "faculty",
		LastUpdated:  time.Now(),
		Created:      time.Now(),
	}

	convo := MessageToConversation(&m)
	convo.SectionId = 42
	err = aytConversations.Insert(convo)
	if err != nil {
		t.Error(err)
	}

	if getConversationIfExists(&m) != nil {
		t.Error("The conversation should not exist yet")
	} else {
		convo := MessageToConversation(&m)
		err := aytConversations.Insert(convo)
		if err != nil {
			t.Error(err)
		}

		if getConversationIfExists(&m) == nil {
			t.Error("The conversation does not exist, but should")
		}
	}
}

func TestQuestionExists(t *testing.T) {
	var err error
	session, err = mgo.Dial("localhost")
	if err != nil {
		t.Error(err)
	}

	aytConversations = session.DB("messages_test").C("aytconversations")
	_ = aytConversations.DropCollection()

	m := Message{
		SectionId:    1,
		DeploymentId: 2,
		StudentId:    3,
		QuestionId:   42,
		To:           "Briana Austin",
		From:         "Andrew Austin",
		Body:         "I'm fine",
		Read:         false,
		Answered:     false,
		Originator:   "faculty",
		LastUpdated:  time.Now(),
		Created:      time.Now(),
	}

	convo := MessageToConversation(&m)
	if !questionExists(convo, &m) {
		t.Error("Question does not exist but it should")
	}

	q2 := Question{
		Id:       17,
		Answered: false,
		Messages: nil,
	}

	convo.Questions = append(convo.Questions, q2)

	if len(convo.Questions) != 2 {
		t.Error("There should be 2 questions")
	}

	if !questionExists(convo, &m) {
		t.Error("Question does not exist but there are two questions and it should")
	}
}

func TestMigrateMessageEmptyCollections(t *testing.T) {
	var err error

	session, err = mgo.Dial("localhost")
	if err != nil {
		t.Error(err)
	}

	aytConversations = session.DB("messages_test").C("aytconversations")
	_ = aytConversations.DropCollection()
	aytMessages = session.DB("messages_test").C("aytmessages")
	_ = aytMessages.DropCollection()

	m := Message{
		Id:           bson.NewObjectId(),
		SectionId:    1,
		DeploymentId: 2,
		StudentId:    3,
		QuestionId:   42,
		To:           "Briana Austin",
		From:         "Andrew Austin",
		Body:         "I'm fine",
		Read:         false,
		Answered:     false,
		Originator:   "faculty",
		LastUpdated:  time.Now(),
		Created:      time.Now(),
	}

	migrateMessageToConversation(&m)
	// The previous funcion call should insert a brand new conversation into the database.
	var dbM Conversation
	err = aytConversations.Find(bson.M{"sectionId": m.SectionId, "deploymentId": m.DeploymentId}).One(&dbM)
	if dbM.SectionId != m.SectionId {
		t.Error("Conversation SectionId does not equal Messages SectionId")
	}

	if dbM.DeploymentId != m.DeploymentId {
		t.Error("Conversation DeploymentId does not equal Message DeploymentId")
	}

	if dbM.StudentId != m.StudentId {
		t.Error("Conversation StudentId does not equal Message StudentId")
	}

	if dbM.LastUpdated.Equal(m.LastUpdated) {
		t.Error("Conversation LastUpdated does not equal Message LastUpdated")
	}

	if dbM.Created.Equal(m.Created) {
		t.Error("Conversation Created does not equal Message Created")
	}

	if len(dbM.Questions) != 1 {
		t.Error("The number of questions should be 1")
	}

	if dbM.Questions[0].Id != m.QuestionId {
		t.Error("The Question Id should match the Message QuestionId")
	}

	if dbM.Questions[0].Answered != m.Answered {
		t.Error("Question Answered should match Message Answered")
	}

	if len(dbM.Questions[0].Messages) != 1 {
		t.Error("Only 1 Message should have been converted")
	}

	if dbM.Questions[0].Messages[0].To != m.To {
		t.Error("The QuestionMessage To field should match Message To field")
	}

	if dbM.Questions[0].Messages[0].From != m.From {
		t.Error("From field should be the same")
	}

	if dbM.Questions[0].Messages[0].Body != m.Body {
		t.Error("The body field should be the same")
	}

	if dbM.Questions[0].Messages[0].Read != m.Read {
		t.Error("The read flag should be the same")
	}

	if dbM.Questions[0].Messages[0].Originator != m.Originator {
		t.Error("Originator field should match")
	}

	if dbM.Questions[0].Messages[0].LastUpdated.Equal(m.LastUpdated) {
		t.Error("Last Updated should match")
	}

	if dbM.Questions[0].Messages[0].Created.Equal(m.Created) {
		t.Error("Created field should match")
	}
}

func TestMigrateMessageExistingConversationExists(t *testing.T) {
	var err error

	session, err = mgo.Dial("localhost")
	if err != nil {
		t.Error(err)
	}

	aytConversations = session.DB("messages_test").C("aytconversations")
	_ = aytConversations.DropCollection()
	aytMessages = session.DB("messages_test").C("aytmessages")
	_ = aytMessages.DropCollection()

	m := Message{
		Id:           bson.NewObjectId(),
		SectionId:    1,
		DeploymentId: 2,
		StudentId:    3,
		QuestionId:   42,
		To:           "Briana Austin",
		From:         "Andrew Austin",
		Body:         "I'm fine",
		Read:         false,
		Answered:     false,
		Originator:   "faculty",
		LastUpdated:  time.Now(),
		Created:      time.Now(),
	}

	migrateMessageToConversation(&m)

	// create a new message in the same conversation, about the same question
	m.Id = bson.NewObjectId()
	m.To = "Andrew Austin"
	m.From = "Briana Austin"
	m.LastUpdated = time.Now()
	m.Created = time.Now()

	migrateMessageToConversation(&m)

	// At this point we should have one converation with one question, but two QuestionMessages
	var dbM Conversation
	err = aytConversations.Find(bson.M{"sectionId": m.SectionId, "deploymentId": m.DeploymentId}).One(&dbM)
	if dbM.SectionId != m.SectionId {
		t.Error("Conversation SectionId does not equal Messages SectionId")
	}

	if dbM.DeploymentId != m.DeploymentId {
		t.Error("Conversation DeploymentId does not equal Message DeploymentId")
	}

	if dbM.StudentId != m.StudentId {
		t.Error("Conversation StudentId does not equal Message StudentId")
	}

	if dbM.LastUpdated.Equal(m.LastUpdated) {
		t.Error("Conversation LastUpdated does not equal Message LastUpdated")
	}

	if dbM.Created.Equal(m.Created) {
		t.Error("Conversation Created does not equal Message Created")
	}

	if len(dbM.Questions) != 1 {
		t.Error("The number of questions should be 1")
	}

	if dbM.Questions[0].Id != m.QuestionId {
		t.Error("The Question Id should match the Message QuestionId")
	}

	if len(dbM.Questions[0].Messages) != 2 {
		t.Error("2 Message should have been converted")
	}

	if dbM.Questions[0].Messages[1].To != m.To {
		t.Error("The QuestionMessage To field should match Message To field")
	}

	if dbM.Questions[0].Messages[0].To != "Briana Austin" {
		t.Error("The To field does not match the correct value of the original message")
	}

	if dbM.Questions[0].Messages[1].LastUpdated.Equal(m.LastUpdated) {
		t.Error("Last Updated should match")
	}

	if dbM.Questions[0].Messages[1].Created.Equal(m.Created) {
		t.Error("Created field should match")
	}
}

func TestMigrateMessageExistingConversationExistsNewQuestion(t *testing.T) {
	var err error

	session, err = mgo.Dial("localhost")
	if err != nil {
		t.Error(err)
	}

	aytConversations = session.DB("messages_test").C("aytconversations")
	_ = aytConversations.DropCollection()
	aytMessages = session.DB("messages_test").C("aytmessages")
	_ = aytMessages.DropCollection()

	m := Message{
		Id:           bson.NewObjectId(),
		SectionId:    1,
		DeploymentId: 2,
		StudentId:    3,
		QuestionId:   42,
		To:           "Briana Austin",
		From:         "Andrew Austin",
		Body:         "I'm fine",
		Read:         false,
		Answered:     false,
		Originator:   "faculty",
		LastUpdated:  time.Now(),
		Created:      time.Now(),
	}

	migrateMessageToConversation(&m)

	// create a new message in the same conversation, but in a different question
	m.Id = bson.NewObjectId()
	m.QuestionId = 1
	m.To = "Andrew Austin"
	m.From = "Briana Austin"
	m.LastUpdated = time.Now()
	m.Created = time.Now()

	migrateMessageToConversation(&m)

	// At this point we should have one converation with one question, but two QuestionMessages
	var dbM Conversation
	err = aytConversations.Find(bson.M{"sectionId": m.SectionId, "deploymentId": m.DeploymentId}).One(&dbM)
	if dbM.SectionId != m.SectionId {
		t.Error("Conversation SectionId does not equal Messages SectionId")
	}

	if dbM.DeploymentId != m.DeploymentId {
		t.Error("Conversation DeploymentId does not equal Message DeploymentId")
	}

	if dbM.StudentId != m.StudentId {
		t.Error("Conversation StudentId does not equal Message StudentId")
	}

	if dbM.LastUpdated.Equal(m.LastUpdated) {
		t.Error("Conversation LastUpdated does not equal Message LastUpdated")
	}

	if dbM.Created.Equal(m.Created) {
		t.Error("Conversation Created does not equal Message Created")
	}

	if len(dbM.Questions) != 2 {
		t.Error("The number of questions should be 2")
	}

	if dbM.Questions[0].Id != 42 {
		t.Error("The Question Id should match the Message QuestionId")
	}

	if dbM.Questions[1].Id != m.QuestionId {
		t.Error("The Question Id should match the Message QuestionId")
	}

	if len(dbM.Questions[0].Messages) != 1 {
		t.Error("1 Message should have been converted")
	}

	if len(dbM.Questions[1].Messages) != 1 {
		t.Error("1 Message should have been converted")
	}

	if dbM.Questions[1].Messages[0].To != m.To {
		t.Error("The QuestionMessage To field should match Message To field")
	}

}

func TestMigrationDates(t *testing.T) {
	var err error

	session, err = mgo.Dial("localhost")
	if err != nil {
		t.Error(err)
	}

	aytConversations = session.DB("messages_test").C("aytconversations")
	_ = aytConversations.DropCollection()
	aytMessages = session.DB("messages_test").C("aytmessages")
	_ = aytMessages.DropCollection()

	m := Message{
		Id:           bson.NewObjectId(),
		SectionId:    1,
		DeploymentId: 2,
		StudentId:    3,
		QuestionId:   42,
		To:           "Briana Austin",
		From:         "Andrew Austin",
		Body:         "I'm fine",
		Read:         false,
		Answered:     false,
		Originator:   "faculty",
		LastUpdated:  time.Now().Round(time.Millisecond),
		Created:      time.Now().Round(time.Millisecond),
	}

	originalUpdated := m.LastUpdated
	migrateMessageToConversation(&m)

	// create a new message in the same conversation, but in a different question
	m.Id = bson.NewObjectId()
	m.QuestionId = 1
	m.To = "Andrew Austin"
	m.From = "Briana Austin"
	m.LastUpdated = time.Now().Round(time.Millisecond).Add((time.Second * 20000) * -1)
	m.Created = time.Now()
	migrateMessageToConversation(&m)

	// At this point we should have one converation with one question, but two QuestionMessages
	var dbM Conversation
	_ = aytConversations.Find(bson.M{"sectionId": m.SectionId, "deploymentId": m.DeploymentId}).One(&dbM)

	if dbM.SectionId != m.SectionId {
		t.Error("Conversation SectionId does not equal Messages SectionId")
	}

	// The conversation time should not be updated because the second message was before the first
	if !dbM.LastUpdated.Equal(originalUpdated) {
		t.Error("Last updated has changed but it should not")
	}

	m.LastUpdated = time.Now().Round(time.Millisecond)
	migrateMessageToConversation(&m)

	_ = aytConversations.Find(bson.M{"sectionId": m.SectionId, "deploymentId": m.DeploymentId}).One(&dbM)
	// Now the time should be updated since the 3rd message is the most recent
	if !dbM.LastUpdated.Equal(m.LastUpdated) {
		t.Error("Converastion LastUpdated does not equal the correct lastUpdated from the most recent message")
	}
}

func TestMigrationReadAnsweredStartFalse(t *testing.T) {
	var err error

	session, err = mgo.Dial("localhost")
	if err != nil {
		t.Error(err)
	}

	aytConversations = session.DB("messages_test").C("aytconversations")
	_ = aytConversations.DropCollection()
	aytMessages = session.DB("messages_test").C("aytmessages")
	_ = aytMessages.DropCollection()

	m := Message{
		Id:           bson.NewObjectId(),
		SectionId:    1,
		DeploymentId: 2,
		StudentId:    3,
		QuestionId:   42,
		To:           "Briana Austin",
		From:         "Andrew Austin",
		Body:         "I'm fine",
		Read:         false,
		Answered:     false,
		Originator:   "faculty",
		LastUpdated:  time.Now().Round(time.Millisecond),
		Created:      time.Now().Round(time.Millisecond),
	}

	migrateMessageToConversation(&m)
	var dbM Conversation
	_ = aytConversations.Find(bson.M{"sectionId": m.SectionId, "deploymentId": m.DeploymentId}).One(&dbM)

	if dbM.Questions[0].Answered != false {
		t.Error("The question should not be answered yet but is")
	}

	if dbM.Questions[0].Messages[0].Read != false {
		t.Error("The question message should not be read yet but it is")
	}

	// create a new message in the same conversation, and the same question
	m.Id = bson.NewObjectId()
	m.QuestionId = 42
	m.To = "Andrew Austin"
	m.From = "Briana Austin"
	m.LastUpdated = time.Now().Round(time.Millisecond).Add((time.Second * 20000) * -1)
	m.Created = time.Now()
	m.Read = true
	m.Answered = true
	migrateMessageToConversation(&m)

	// At this point we should have one converation with one question, but two QuestionMessages
	_ = aytConversations.Find(bson.M{"sectionId": m.SectionId, "deploymentId": m.DeploymentId}).One(&dbM)

	if dbM.Questions[0].Answered != false {
		t.Error("The question should not be answered yet but it is")
	}

	if dbM.Questions[0].Messages[0].Read != false {
		t.Error("The question message should not be read yet but it is")
	}

	if dbM.Questions[0].Messages[1].Read != true {
		t.Error("The question message should be read but it is not")
	}
}

func TestMigrationReadAnsweredStartTrue(t *testing.T) {
	var err error

	session, err = mgo.Dial("localhost")
	if err != nil {
		t.Error(err)
	}

	aytConversations = session.DB("messages_test").C("aytconversations")
	_ = aytConversations.DropCollection()
	aytMessages = session.DB("messages_test").C("aytmessages")
	_ = aytMessages.DropCollection()

	m := Message{
		Id:           bson.NewObjectId(),
		SectionId:    1,
		DeploymentId: 2,
		StudentId:    3,
		QuestionId:   42,
		To:           "Briana Austin",
		From:         "Andrew Austin",
		Body:         "I'm fine",
		Read:         true,
		Answered:     true,
		Originator:   "faculty",
		LastUpdated:  time.Now().Round(time.Millisecond),
		Created:      time.Now().Round(time.Millisecond),
	}

	migrateMessageToConversation(&m)
	var dbM Conversation
	_ = aytConversations.Find(bson.M{"sectionId": m.SectionId, "deploymentId": m.DeploymentId}).One(&dbM)

	if dbM.Questions[0].Answered != true {
		t.Error("The question should be answered yet but it is not")
	}

	if dbM.Questions[0].Messages[0].Read != true {
		t.Error("The question message should be read yet but it is not")
	}

	// create a new message in the same conversation, and the same question
	m.Id = bson.NewObjectId()
	m.QuestionId = 42
	m.To = "Andrew Austin"
	m.From = "Briana Austin"
	m.LastUpdated = time.Now().Round(time.Millisecond).Add((time.Second * 20000) * -1)
	m.Created = time.Now()
	m.Read = false
	m.Answered = false
	migrateMessageToConversation(&m)

	// At this point we should have one converation with one question, but two QuestionMessages
	_ = aytConversations.Find(bson.M{"sectionId": m.SectionId, "deploymentId": m.DeploymentId}).One(&dbM)

	if dbM.Questions[0].Answered != false {
		t.Error("The question should not be answered yet but it is")
	}

	if dbM.Questions[0].Messages[0].Read != true {
		t.Error("The question message should be read yet but it is not")
	}

	if dbM.Questions[0].Messages[1].Read != false {
		t.Error("The question message should not be read but it is")
	}
}
